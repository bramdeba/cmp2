<?php
	get_header(); 
?>
<div class="content">
	<div class="container blog">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
		    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnail' );
			$url = $thumb['0'];
		   	?>
			
			<article>
				<a href="<?php the_permalink(); ?>"><div class="<?php if (empty($url)){echo 'no-';} ?>cover-image" style="background-image:url(<?= $url ?>)">
				</div>
				<h2><?php the_date(); ?></h2>
				<h1><?php the_title(); ?></h1></a>
			</article>

		   <?php endwhile; else : ?>
			<p><?php _e( 'Sorry, no posts available.' ); ?></p>
			<?php endif; ?>
			
			<div class="navigation"><p class="prev">&nbsp;<?php previous_posts_link('&laquo; Previous page'); ?></p><p class="next"><?php next_posts_link('Next page &raquo;'); ?>&nbsp;</p></div>
	</div>
</div>
<?php get_footer(); ?>