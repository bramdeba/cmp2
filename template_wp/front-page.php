<?php
	get_header(); 
?>
<div class="content">
	<div class="container blog">
		<header><h1>Latest news</h1></header>
		<?php $recent = new WP_Query("showposts=3"); ?>
		<?php if ( $recent->have_posts() ) : while ( $recent->have_posts() ) : $recent->the_post();
		    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnail' );
			$url = $thumb['0'];
		   	?>
			
			<article>
				<a href="<?php the_permalink(); ?>"><div class="<?php if (empty($url)){echo 'no-';} ?>cover-image" style="background-image:url(<?= $url ?>)">
				</div>
				<h2><?php the_date(); ?></h2>
				<h1><?php the_title(); ?></h1></a>
			</article>

		   <?php endwhile; else : ?>
			<p><?php _e( 'Sorry, no news available.' ); ?></p>
			<?php endif; ?>
	</div>
</div>
<div class="side_button_wrapper">
	<i id="twitter_button" class="fa fa-twitter"></i>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>