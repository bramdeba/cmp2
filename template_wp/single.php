<?php get_header(); ?>
<div class="content">
	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnail' );
	$url = $thumb['0']; ?>
	<div class="<?php if (empty($url)){echo 'no-';} ?>cover-image" style="background-image:url(<?= $url ?>)">
	</div>
	<div class="container blog">
		<article class="blog-post">
			<?php if(have_posts()): while(have_posts()): the_post(); ?>
			<h2><?php the_date(); ?><?php if(is_user_logged_in()){?> - <?php }?><?php edit_post_link(__('Edit Post')); ?></h2>
			<h1><?php the_title(); ?></h1>
		    <?php the_content(); ?>
		    <?php echo get_the_tag_list('<p class="tags"><small><i class="fa fa-tag"></i></small> ','<small>, </small>','</p>'); ?>
			<?php endwhile; endif; ?>
		</article>
		<?php comments_template(); ?>
	</div>
</div>
<?php get_footer(); ?>