<aside id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
	<div class="slimScroll">
	<?php if (is_front_page()){?>
		<?php dynamic_sidebar( 'twitter_side' ); ?>
	<?php } else { ?>
		<?php dynamic_sidebar( 'offcanvas_side' ); ?>
	<?php } ?>
	</div>
</aside>