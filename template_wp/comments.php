<article id="comment-section">
	<h1>Comments</h1>
	<ol class="comment-list">
		<?php wp_list_comments('type=comment&callback=project_comment'); ?> 
		<li>
			<article class="comment-body">
				<?php comment_form(array('comment_field'=>'
					<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>
				')); ?>
			</article><!-- .comment-body -->
		</li><!-- #comment-## -->
	</ol>
</article>