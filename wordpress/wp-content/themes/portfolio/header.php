<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
		<title><?php if (is_front_page()) { echo 'Home'; } else if (is_post_type_archive('project')){ echo 'Portfolio'; } else if (is_home()) { echo 'Blog'; } else if (is_archive()){ echo 'Posts'; } else { the_title(); }?> | <?php bloginfo('name'); ?></title>
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicon-194x194.png" sizes="194x194">
		<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/android-chrome-192x192.png" sizes="192x192">
		<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="<?php bloginfo('template_url'); ?>/manifest.json">
		<link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/safari-pinned-tab.svg" color="#dd4f82">
		<meta name="msapplication-TileColor" content="#b91d47">
		<meta name="msapplication-TileImage" content="<?php bloginfo('template_url'); ?>/mstile-144x144.png">
		<meta name="theme-color" content="#2c2c2c">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,400italic,300italic|Oswald:400,300' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css">
		<?php wp_head(); ?>
	</head>
	<?php if (is_front_page()){ ?>
	<body id="home" class="overview menu-closed side-closed">
	<?php } else if (is_home()||is_archive()){ ?>
	<body class="overview menu-closed side-closed">
	<?php } else { ?>
	<body class="menu-closed side-closed">
	<?php } ?>
		<nav>
			<ul>
				<?php wp_nav_menu( array('theme_location' => 'header-menu')); ?>
			</ul>
			<div class="nav-bottom">
				<ul>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
				</ul>
				<ul id="icon-row"><div>
					<a href="http://www.facebook.com/bramdeback"><li><i class="fa fa-facebook"></i></li></a>
					<a href="http://twitter.com/BramDeBacker"><li><i class="fa fa-twitter"></i></li></a>
					<a href="<?php bloginfo('atom_url'); ?>"><li><i class="fa fa-rss"></i></li></a>
					<a href="mailto:bram.debacker@student.arteveldehs.be"><li><i class="fa fa-envelope"></i></li></a>
				</div></ul>
				<ul id="copy">
					<?php if (is_user_logged_in()){ ?>
						<li><small><a class="link_login" href="<?= admin_url() ?>">Projectmanager</a> - <a class="link_login" href="<?= wp_logout_url( get_permalink() ); ?>">Logout</a></small></li>
					<?php } else { ?>
						<li><small>&copy; 2016 - <?php bloginfo('name'); ?><a class="link_login" href="<?= wp_login_url( get_permalink() ); ?>"> [Login]</a></small></li>
					<?php } ?>
				</ul>
			</div>
		</nav>
		<div class="content-wrapper">
			<div class="header">
			<!--<h1>Bram De Backer</h1>-->
			<img id="logo" src="<?php bloginfo('template_url'); ?>/images/logo.svg" alt="<?php bloginfo('name'); ?>">
			<h2>&#126; <?php bloginfo('description'); ?> &#126;</h2>
		</div>
		<i id="open_button" class="fa fa-bars"></i>