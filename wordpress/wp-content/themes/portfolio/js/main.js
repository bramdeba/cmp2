$(document).ready(function(){
    var slimScroll = $('.slimScroll');
    if (slimScroll!==null){
        slimScroll.slimScroll({
            height: '100%',
            distance: '0.4rem',
            size:'.15rem',
            opacity:'0.25',
            color:'#8C8C8C',
            //color:'#419BEC',
            allowPageScroll: true
        });
    }

    var aside = document.querySelector('aside');
    if (aside!==null) {
        var asideBtn = document.querySelector('.side_button_wrapper');
        var top = aside.offsetTop;
        var listener = function () {
            var y = window.pageYOffset;

            if (y >= top) {
                aside.classList.add('stick');
                asideBtn.classList.add('stick');
                custom.allowPageScroll = false
                custom.allowScroll = true;
            } else {
                aside.classList.remove('stick');
                asideBtn.classList.remove('stick');
                custom.allowPageScroll = true;
                custom.allowScroll = false;
                //$('.slimScroll').slimScroll({allowPageScroll: true});
            }
        };
        window.addEventListener('scroll', listener, false);
    }
    if ($('.side_button_wrapper').length!==0){
        document.querySelector('.side_button_wrapper').onclick = function(){
            var body = document.getElementsByTagName('body')[0];
            if (body.className.indexOf('side-open')!==-1){
                body.className = body.className.replace('side-open','side-closed');
            } else {
                body.className = body.className.replace('side-closed','side-open');
            }
        }
    }

    document.querySelector('#open_button').onclick = function(){
        var body = document.getElementsByTagName('body')[0];
        if (body.className.indexOf('menu-open')!==-1){
            body.className = body.className.replace('menu-open','menu-closed');
        } else {
            body.className = body.className.replace('menu-closed','menu-open');
            body.className = body.className.replace('side-open','side-closed')
        }
    }

    var count = 0;

    var msnry = new Masonry('#masonry', {
        itemSelector: 'article.grid-item',
        columnWidth: '.grid-sizer',
        gutter: '.gutter-sizer',
        percentPosition: true,
        initLayout: false
    });

    msnry.on('layoutComplete', function(){

        if (count===0) {
            count++;

            imagesLoaded('#masonry').on( 'progress', function( instance, image ) {
                msnry.layout();
            });
        }
    });

    msnry.layout();
});