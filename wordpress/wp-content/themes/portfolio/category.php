<?php get_header(); ?>
<div class="content">
	<div class="container">
		<header><h1><?php single_cat_title(); ?></h1></header>
		<section id="masonry">
			<article class="grid-sizer"></article>
			<article class="gutter-sizer"></article>
			<?php
				$wp_query->query_vars["posts_per_page"] = 9999;
				$wp_query->get_posts();
			?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
			    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'portfolio-thumb' );
				$url = $thumb['0'];
			   	?>
			<article class="grid-item">
				<a href="<?php the_permalink(); ?>"><img src="<?= $url ?>"
				alt="<?php the_title(); ?>"></a>
				<div>
					<a href="<?php the_permalink(); ?>"><h1><?php the_title(); ?></h1></a>
					<p class="description">
						<?php the_excerpt(); ?>
					</p>
				</div>
			</article>
			<?php endwhile; else : ?>
			<p><?php _e( 'Sorry, no projects available.' ); ?></p>
			<?php endif; ?>
		</section>
	</div>
</div>
<div class="side_button_wrapper">
	<i id="twitter_button" class="fa fa-filter"></i>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>