<?php get_header(); ?>
<div class="content">
	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnail' );
	$url = $thumb['0']; ?>
	<div class="<?php if (!empty($url)){echo 'cover-image';} ?>" style="background-image:url(<?= $url ?>)">
	</div>
	<div class="container page">
		<article class="page-post">
			<h1>OOPS</h1>
			<h2>Nothing here to see, goodbye!</h2>
		</article>
	</div>
</div>
<?php get_footer(); ?>