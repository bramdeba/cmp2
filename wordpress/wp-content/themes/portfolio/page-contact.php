<?php get_header(); ?>
<div class="content">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnail' );
	$url = $thumb['0']; ?>
	<div class="cover-image">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d80367.47695799518!2d3.4421104786429586!3d50.98873406158367!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c36b61bfa4f6bd%3A0xaf21a4839c7352b7!2sDeinze!5e0!3m2!1snl!2sbe!4v1461178380284" frameborder="0" style="border:0; width:100%;height:100%" allowfullscreen></iframe>
	</div>
	<div class="container page">
		<article class="blog-post">
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</article>
	</div>
	<?php endwhile; endif; ?>
</div>
<?php get_footer(); ?>