<?php get_header(); ?>
<div class="content">
	<div class="container page">
		<?php the_post_thumbnail('portfolio-thumb'); ?>
		<article class="blog-post">
			<?php if(have_posts()): while(have_posts()): the_post(); ?>
			<h1><?php the_title(); ?><?php if (is_user_logged_in()){ echo ' - '; edit_post_link(__('<small>Edit Post</small>'));} ?></h1>
		    <?php the_content(); ?>
		    <?php if (get_field('external_link')){ ?><a class="button" target="_blank" href="<?php the_field('external_link'); ?>">Let me go there!</a><?php } ?>
		    <?=function_exists('thumbs_rating_getlink') ? thumbs_rating_getlink() : ''?>
		    <?php echo get_the_tag_list('<p class="tags"><small><i class="fa fa-tag"></i></small> ','<small>, </small>','</p>'); ?>
			<?php endwhile; endif; ?>
		</article>
		<?php comments_template(); ?>
		<article class="forum blog-post">
			<?= do_shortcode('[bbp-forum-index]'); ?>
		</article>
	</div>
</div>
<?php get_footer(); ?>