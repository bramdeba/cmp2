<?php get_header(); ?>
<div class="content">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnail' );
	$url = $thumb['0']; ?>
	<div class="<?php if (!empty($url)){echo 'cover-image';} ?>" style="background-image:url(<?= $url ?>)">
	</div>
	<div class="container page">
		<article class="blog-post">
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</article>
	</div>
	<?php endwhile; endif; ?>
</div>
<?php get_footer(); ?>