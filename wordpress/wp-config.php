<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '');

/** MySQL database username */
define('DB_USER', '');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', '');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'KGzU8O6Deq>W/Jf-6yV8*<^vTXCU![wUXylCa)k|}3g@X|~99m/sH-}<l!pr`d}E');
define('SECURE_AUTH_KEY',  'oY+It5m+mNu#K|1WHhcS,7<a>QF|8+a.o|Phg*b!dY8Cnbbq{8w8Q}5eP_VANfF,');
define('LOGGED_IN_KEY',    'r&Jp0cTl+Ud(smUp~H+`C{Dsogj0m{C,kXEk7+%KLMv[`hVUS_6cV6ON>PhROioT');
define('NONCE_KEY',        '?^)tIFG35wbH|bgu{G_IwgBOd)MhU,X4`GkXj+&2|O=?.GTEt2LeFz1`mY+FFT`i');
define('AUTH_SALT',        'E]|SLroX<Ou$UO:M^Z |i|eDN-PGD4!cq3pYs>vCJV7nOp2=:.][u% sY]w%e!f0');
define('SECURE_AUTH_SALT', '&b--f}qV9=9/~4W-x+^-O,xs74/f_:]>r9b6%/ 05]^>NJ$jhDILTvS%<ib`4&MI');
define('LOGGED_IN_SALT',   '||nN9wx#hl~c[fuf2K$GoD,2K)!+-^,M_n)|u#Xu.gWAJ|&=EWTilpCc &a>+.$-');
define('NONCE_SALT',       '+eTYze(l[:=P9a>aelapLsGc`B/wnGdxnOQQbJQk/Z*Wf>B(d)W=Jy-a770]Q=41');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
